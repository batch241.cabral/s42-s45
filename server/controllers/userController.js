import User from "../models/Users.js";
import Product from "../models/Products.js";
import {
    userRegisterValidation,
    userLoginValidation,
    userUpdateValidation,
} from "../validation/userInputValidation.js";
import bcrypt from "bcrypt";
import { createAccessToken, decode } from "../validation/auth.js";

async function userRegistration(req, res) {
    const { error } = userRegisterValidation(req.body);
    const user = await User.findOne({ email: req.body.email });

    try {
        //Validate the incoming request body using "userRegisterValidation" function. If the validation fails, it returns a 400 error with the first validation error message.
        if (error) return res.status(400).send({ message: error.details[0].message });

        //Check if the provided email is already registered by querying the User collection with the same email.
        if (user)
            return res.status(400).send({
                message: "Email is already registered please log in instead.",
            });

        //If the email is not registered, it generates a salt using the SALT environment variable and uses bcrypt to hash the password.
        const salt = await bcrypt.genSalt(Number(process.env.SALT));
        const hashPassword = await bcrypt.hash(req.body.password, salt);

        //Save the new user with the hashed password in the User collection.
        await new User({ ...req.body, password: hashPassword }).save();
        return res.status(201).send({ message: "User created successfully." });
    } catch (err) {
        return res.status(500).send({ message: err.message });
    }
}

async function userLogin(req, res) {
    const user = await User.findOne({ email: req.body.email });

    try {
        if (!user) return res.status(404).send({ message: "User not found." });

        // Compares the password from the request body to the hashed password stored in the database using the bcrypt library's compareSync function.
        const validPassword = bcrypt.compareSync(req.body.password, user.password);
        if (!validPassword)
            return res.status(400).send({ message: "Password incorrect." });

        //If the email and password are valid, it creates a JSON Web Token
        createAccessToken(user);
        return res.status(201).send({ message: createAccessToken(user) });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}

async function userDetails(req, res) {
    const { userId } = decode(req.headers.authorization);
    const user = await User.findById({ _id: userId });

    try {
        //Decodes the JSON Web Token (JWT) passed in the authorization header to extract the user ID
        if (!user) return res.status(404).send({ message: "User not found." });
        return res.status(200).send({ user });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}
async function updateDetails(req, res) {
    const { userId } = decode(req.headers.authorization);
    const { error } = userUpdateValidation(req.body);

    try {
        if (error) return res.status(400).send({ message: error.details[0].message });

        const updates = {};
        Object.entries(req.body).forEach(([key, value]) => {
            if (["firstName", "lastName", "email", "password", "address"].includes(key)) {
                updates[key] = value;
            }
            if (key === "address") {
                updates.address = {};
                Object.entries(value).forEach(([addressKey, addressValue]) => {
                    if (["street", "city", "province", "zipcode"].includes(addressKey)) {
                        updates.address[addressKey] = addressValue;
                    }
                });
            }
        });

        if (req.body.password) {
            const salt = await bcrypt.genSalt(Number(process.env.SALT));
            const hashPassword = await bcrypt.hash(req.body.password, salt);
            updates.password = hashPassword;
        }

        //Queries the database for a product with the specified ID and updates it with the filtered fields.
        const user = await User.findOneAndUpdate({ _id: userId }, updates, {
            new: true,
        });

        if (!user)
            return res
                .status(404)
                .send({ message: "User not found, please log in again." });

        return res.status(200).send({
            message: `${user.firstName}'s details updated successfully.`,
        });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}
async function createAdmin(req, res) {
    const { isAdmin } = decode(req.headers.authorization);
    const userId = req.params.id;
    const user = await User.findById({ _id: userId });

    try {
        if (!isAdmin)
            return res.status(401).send({ message: "Current user is not authorized." });

        if (!user)
            return res.status(404).send({ message: "User not found in the database." });

        //If the user is already an admin, it returns a 400 Bad Request error with a message indicating that the user is already an admin.
        if (user.isAdmin)
            return res
                .status(400)
                .send({ message: `${user.firstName} is currently an admin.` });
        user.isAdmin = true;
        await user.save();
        return res.status(200).send({ message: `${user.firstName} is now an admin.` });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}

async function viewAuthenticatedOrders(req, res) {
    const { userId } = decode(req.headers.authorization);
    const user = await User.findById({ _id: userId }, { orders: 1, cartTotal: 1 });

    try {
        if (!userId) return res.status(404).send({ message: "Invalid token" });

        if (!user)
            return res.status(404).send({ message: "User not found in the database." });

        if (!user.orders.length)
            return res
                .status(404)
                .send({ message: "No order found, your cart is empty." });
        return res.status(200).send({ orders: user });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}

async function viewAllOrders(req, res) {
    const { isAdmin } = decode(req.headers.authorization);
    const user = await User.find({ checkOutDetails: { $ne: [] } });

    try {
        if (!isAdmin) return res.status(401).send({ message: "User not authorized." });

        if (!user.length)
            return res
                .status(400)
                .send({ message: "There is no current order in any account." });
        return res.status(200).send({ orders: user });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}

async function checkOutOrders(req, res) {
    const { isAdmin, userId } = decode(req.headers.authorization);
    const user = await User.findById({ _id: userId });

    try {
        if (isAdmin)
            return res
                .status(401)
                .send({ message: "Admin is not authorized to checkout." });

        if (!user)
            return res.status(404).send({ message: "User not found in the database." });

        if (!user.orders.length)
            return res.status(404).send({ message: "No item in the cart to check out." });

        let total = 0;
        let quantitiesSum = 0;

        for (let i = 0; i < user.orders.length; i++) {
            const order = user.orders[i];
            const productName = order.products[0].productName;

            for (let j = 0; j < order.products.length; j++) {
                const product = await Product.findOne({ productName: productName });

                if (!product || product.stockCount == 0) {
                    product.isActive = false;
                    return res.status(400).send({
                        message: `${productName} is not available, please remove this from your cart.`,
                    });
                }

                const quantity = parseInt(JSON.stringify(order.products[j].quantity));
                quantitiesSum += quantity;

                // Deducts the requested quantity from the product's "stockCount"
                product.stockCount -= quantity;
                product.soldCount += quantity;
                await product.save();
            }
            total += order.totalAmount;
            user.checkOutDetails.push({
                productName: productName,
                quantity: quantitiesSum,
                checkOutTotal: order.totalAmount,
            });
            quantitiesSum = 0;
        }

        // Finally, the function removes all items from the user's cart by resetting the orders and cartTotal fields to an empty array and zero, respectively.
        user.orders = [];
        user.cartTotal = 0;
        await user.save();

        return res.status(201).send({
            message:
                "Purchased items will be delivered to your registered address. THANK YOU.",
        });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}

async function removeAuthenticatedOrders(req, res) {
    const { userId } = decode(req.headers.authorization);
    const orderId = req.params.id;
    const user = await User.findById({ _id: userId });

    try {
        if (!userId) return res.status(401).send({ message: "User not authorized" });

        //Finds the user in the database based on the userId obtained from the decoded token.
        if (!user) return res.status(404).send({ message: "User not found" });

        let total = 0;
        for (let i = 0; i < user.orders.length; i++) {
            const order = user.orders[i];
            total += order.totalAmount;
        }

        //Finds the order to remove based on the orderId and calculates the total amount of that order.
        const order = user.orders.find((order) => order.id === orderId);
        if (!order) {
            return res.status(404).send({ message: "Order not found" });
        }

        //Filters the user's orders array and removes the order with the matching id.
        const updatedOrders = user.orders.filter((order) => order.id !== orderId);

        if (updatedOrders === []) {
            return res.status(404).send({ message: "Empty cart" });
        }

        //Updates the user's cartTotal by subtracting the total amount of the removed order.
        user.cartTotal -= total;
        user.orders = updatedOrders;
        await user.save();
        return res.status(200).send({ message: "Order successfully removed" });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}
async function viewcheckOutOrders(req, res) {
    const { userId } = decode(req.headers.authorization);
    try {
        const user = await User.findOne({ _id: userId }, { checkOutDetails: 1 }).exec();
        if (!user) {
            return res.status(404).send({ message: "User not found" });
        }
        return res.status(200).send({ orders: user.checkOutDetails });
    } catch (error) {
        return res.status(500).send({ message: error.message });
    }
}

export {
    userRegistration,
    userLogin,
    userDetails,
    createAdmin,
    viewAuthenticatedOrders,
    viewAllOrders,
    checkOutOrders,
    removeAuthenticatedOrders,
    updateDetails,
    viewcheckOutOrders,
};
